<%-- 
    Document   : registro
    Created on : Feb 7, 2016, 2:20:16 PM
    Author     : cocoy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <title>JSP Page</title>
        <style>
            .column{
                float: left;
            }
            #formulario{
                width: 20%;
            }
            #tabla{
                width: 80%;
            }
            #registroPersona input{
                display: block;
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $("#btnSave").click(function () {
                    $.ajax({
                        url: "usuario",
                        method: "post",
                        dataType: "json",
                        data: {
                            nombre: $("#nombreInp").val(),
                            paterno: $("#paternoInp").val(),
                            materno: $("#maternoInp").val()
                        }
                    }).done(function (data) {
                        alert("data.ok");
                    });
                    
                });
            });
        </script>
    </head>
    <body>
        <h1>registro!</h1>
        <div class="column" id="formulario">
            <form id="registroPersona">
                <input id="nombreInp" placeholder="Nombre"/>
                <input id="paternoInp" placeholder="Paterno"/>
                <input id="maternoInp" placeholder="Materno"/>
                <input id="btnSave" type="button" value="Guardar"/>                
            </form>
        </div>
        <div class="column" id="tabla">
            <table id="usuarios" border="1">
                <tr>
                    <td>Nombre</td>
                    <td>Paterno</td>
                    <td>Materno</td>
                </tr>
            </table>
        </div>
    </body>
</html>
