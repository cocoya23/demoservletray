package org.eja.demoservlet.servlet;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executor;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "IndexServlet", urlPatterns = {"/"})
public class IndexServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        String nuevo = req.getParameter("extra");
        
        System.out.println("IndexServlet="+nuevo);
        
        if(nuevo == null){
            RequestDispatcher dispatcher = req.getRequestDispatcher("index.jsp");
            dispatcher.forward(req, resp);
        } else {
            RequestDispatcher dispatcher = req.getRequestDispatcher("registro.jsp");
            dispatcher.forward(req, resp);
        }
        
    }

   
    
    
}
