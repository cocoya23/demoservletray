
package org.eja.demoservlet.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eja.demoservlet.dto.UsuarioDTO;
import org.eja.demoservlet.service.UsuariosService;

@WebServlet(name = "UsuariosServlet", urlPatterns = {"/usuario"})
public class UsuariosServlet extends HttpServlet{
    
    UsuariosService usuariosService = new UsuariosService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        System.out.println("UsuariosServlet");
        
        String nombre = req.getParameter("nombre");
        String paterno = req.getParameter("paterno");
        String materno = req.getParameter("materno");
        
        UsuarioDTO usuarioDTO = new UsuarioDTO(nombre, paterno, materno);
        
        usuariosService.saveUser(usuarioDTO);
        
        /*
        Enumeration<String> parameterNames = req.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String nextElement = parameterNames.nextElement();
            System.out.println(nextElement + "=" + req.getParameter(nextElement));
        }
        */
        
        resp.setContentType("application/json");
        
        PrintWriter writer = resp.getWriter();
        
        writer.println("{ ok: true, msg: '', usuario:{ id: 0, nombre: '"+nombre+"' , paterno: '"+paterno+"' , materno: '"+materno+"'} }");
        
    }
    
    
    
}
