/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eja.demoservlet.service;

import org.eja.demoservlet.dao.UsuariosDAO;
import org.eja.demoservlet.dto.UsuarioDTO;
import org.eja.demoservlet.model.Usuario;


public class UsuariosService {
    
    UsuariosDAO usuariosDAO = new UsuariosDAO();
    
    
    public void saveUser(UsuarioDTO usuarioDTO){
        
        Usuario usuario = new Usuario();
        usuario.setNombre(usuarioDTO.getNombre());
        usuario.setPaterno(usuarioDTO.getPaterno());
        usuario.setMaterno(usuarioDTO.getMaterno());
        
        usuariosDAO.save(usuario);
        
    }
    
}
